﻿using Kursova.MyInterface;
using Kursova.MyInterface.SceneElements;
using System;
using System.Windows.Media;

namespace Kursova
{
    internal class GateBuider
    {
        public static LogicGate CreateGate_AND()
        {
            return new LogicGate(2, 1, "AND", new Func<bool[], bool[]>(b => new bool[] { b[0] && b[1] }), Colors.LightGreen);
        }       
        public static LogicGate CreateGate_NOT()
        {
            return new LogicGate(1, 1, "NOT", new Func<bool[], bool[]>(b => new bool[] { !b[0] }), Colors.Azure);
        }       
        public static LogicGate CreateGate_OR()
        {
            return new LogicGate(2, 1, "OR", new Func<bool[], bool[]>(b => new bool[] { b[0] || b[1] }), Colors.SteelBlue);
        }       
        public static LogicGate CreateGate_XOR()
        {
            return new LogicGate(2, 1, "XOR", new Func<bool[], bool[]>(b => new bool[] { b[0] ^ b[1] }), Colors.SandyBrown);
        }       
        public static LogicGate CreateGate_NOR()
        {
            return new LogicGate(2, 1, "NOR", new Func<bool[], bool[]>(b => new bool[] { !(b[0] || b[1]) }), Colors.PaleTurquoise);
        }      
        public static LogicGate CreateGate_NAND()
        {
            return new LogicGate(2, 1, "NAND", new Func<bool[], bool[]>(b => new bool[] { !(b[0] && b[1]) }), Colors.ForestGreen);
        }
        public static LogicGate CreateGate_XNOR()
        {
            return new LogicGate(2, 1, "XNOR", new Func<bool[], bool[]>(b => new bool[] { !(b[0] ^ b[1]) }), Colors.Violet);
        }
        public static LogicGate CreateGate_ADDER()
        {
            return new LogicGate(3, 2, "ADDER", new Func<bool[], bool[]>(b => new bool[] 
            { 
                b[0] ^ b[1] ^ b[2],
                (b[0] && b[1]) || (b[2] && (b[0] ^ b[1]))
            }), 
            Colors.OliveDrab);
        }
        public static LogicGate CreateGate_GPU1()
        {
            return new LogicGate(4, 7, "GPU-1", new Func<bool[], bool[]>(b => new bool[]
            {
                (b[3] || b[2] || b[1] || !b[0]) && (b[3] || !b[2] || b[1] || b[0]),
                (b[3] || !b[2] || b[1] || !b[0]) && (b[3] || !b[2] || !b[1] || b[0]),
                (b[3] || b[2] || !b[1] || b[0]),
                (b[3] || b[2] || b[1] || !b[0]) && (b[3] || !b[2] || b[1] || b[0]) && (b[3] || !b[2] || !b[1] || !b[0]),
                (b[3] || b[2] || b[1] || !b[0]) && (b[3] || b[2] || !b[1] || !b[0]) && (b[3] || !b[2] || b[1] || b[0]) &&
                (b[3] || !b[2] || b[1] || !b[0]) && (b[3] || !b[2] || !b[1] || !b[0]) && (!b[3] || b[2] || b[1] || !b[0]),
                (b[3] || b[2] || b[1] || !b[0]) && (b[3] || b[2] || !b[1] || b[0]) && (b[3] || b[2] || !b[1] || !b[0]) && (b[3] || !b[2] || !b[1] || !b[0]),
                (b[3] || b[2] || b[1] || b[0]) && (b[3] || b[2] || b[1] || !b[0]) && (b[3] || !b[2] || !b[1] || !b[0])
            }), 
            Colors.CornflowerBlue);
        }
        public static Gate CreateDisplay()
        {
            return new SevenDigitDisplay();
        }
        public static Gate CreateCoder()
        {
            return new Coder();
        }
    }
}
