﻿using System.Windows;
using Kursova.MyInterface;

namespace Kursova
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void WireButtonStraight_Checked(object sender, RoutedEventArgs e)
        {
            WireButtonCurved.IsChecked = false;
            scene.WireState = Wire.State.StraightWire;
        }
        private void WireButtonCurved_Checked(object sender, RoutedEventArgs e)
        {
            WireButtonStraight.IsChecked = false;
            scene.WireState = Wire.State.CurvedWire;
        }
        private void WireButton_Unchecked(object sender, RoutedEventArgs e)
        {
            scene.WireState = Wire.State.NoWire;
        }
        private void buttonAddANDgate_Click(object sender, RoutedEventArgs e)
        {
            scene.AddGate(GateBuider.CreateGate_AND());
        }
        private void buttonAddNOTgate_Click(object sender, RoutedEventArgs e)
        {
            scene.AddGate(GateBuider.CreateGate_NOT());
        }
        private void buttonAddORgate_Click(object sender, RoutedEventArgs e)
        {
            scene.AddGate(GateBuider.CreateGate_OR());
        }
        private void buttonAddXORgate_Click(object sender, RoutedEventArgs e)
        {
            scene.AddGate(GateBuider.CreateGate_XOR());
        }
        private void buttonAddNORgate_Click(object sender, RoutedEventArgs e)
        {
            scene.AddGate(GateBuider.CreateGate_NOR());
        }
        private void buttonAddNANDgate_Click(object sender, RoutedEventArgs e)
        {
            scene.AddGate(GateBuider.CreateGate_NAND());
        }
        private void buttonAddXNORgate_Click(object sender, RoutedEventArgs e)
        {
            scene.AddGate(GateBuider.CreateGate_XNOR());
        }
        private void buttonAddADDERgate_Click(object sender, RoutedEventArgs e)
        {
            scene.AddGate(GateBuider.CreateGate_ADDER());
        }    
        private void buttonAddDisplay_Click(object sender, RoutedEventArgs e)
        {
            scene.AddGate(GateBuider.CreateDisplay());
        }       
        private void buttonAddCoder_Click(object sender, RoutedEventArgs e)
        {
            scene.AddGate(GateBuider.CreateCoder());
        }
        private void buttonAddGPU1_Click(object sender, RoutedEventArgs e)
        {
            scene.AddGate(GateBuider.CreateGate_GPU1());
        }
        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            scene.ClearScene();
        }
        private void ButtonInfo_Click(object sender, RoutedEventArgs e)
        {
            InformationPanel infoPanel = new InformationPanel();
            infoPanel.ShowDialog();
        }
    }
}
