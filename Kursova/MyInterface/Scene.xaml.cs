﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kursova.MyInterface
{
    /// <summary>
    /// Interaction logic for Scene.xaml
    /// </summary>
    public partial class Scene : UserControl
    {
        public Scene()
        {
            InitializeComponent();
        }

        UIElement dragObject;
        UIElement contextMenuCaller;
        Wire drawWire;
        Point dragOffset;

        private Wire.State wireState;

        public Wire.State WireState
        {
            get { return wireState; }
            set { wireState = value; }
        }

        private void SideCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (SideCanvas_NearPins(sender, e) || SideCanvas_CheckOutOfBounds(sender, e))
                return;

            if (e.LeftButton != MouseButtonState.Pressed)
                return;

            UniversalPin pin = new UniversalPin(false, 40);
            (sender as Canvas).Children.Add(pin);

            pin.PreviewMouseDown += CanvasElement_PreviewMouseDown;
            pin.MouseLeave += Pin_NotActive;
            pin.MouseDown += UniversalPinRemovePin_MouseDown;

            if (sender == canvasPinInput)
            {
                Canvas.SetRight(pin, 0);
                pin.MouseDown += UniversalPinChangeState_MouseDown;               
                pin.MouseDown += Pin_StartWire;
                pin.MouseUp += Pin_RemoveWire;
            }
            if (sender == canvasPinOutput)
            {
                pin.MouseEnter += Pin_PreviewMouseEnter;
            }

            Canvas.SetTop(pin, e.GetPosition(sender as IInputElement).Y - 20);
        }
        private bool SideCanvas_NearPins(object sender, MouseEventArgs e)
        {
            if (sender is Canvas cav)
            {
                Point pos = e.GetPosition(sender as IInputElement);
                for (int i = 0; i < cav.Children.Count; i++)
                {
                    if (Math.Abs(Canvas.GetTop(cav.Children[i]) - pos.Y) <= 20
                        || Math.Abs(pos.Y - Canvas.GetTop(cav.Children[i]) - 40) <= 20)
                        return true;
                }
            }
            return false;
        }
        private bool SideCanvas_CheckOutOfBounds(object sender, MouseEventArgs e)
        {
            if (sender is Canvas cav)
            {
                double pinSize = 40;
                Point pos = e.GetPosition(cav);
                if (pos.Y <= pinSize / 2 || pos.Y + pinSize - 10 >= cav.ActualHeight)
                    return true;
            }
            return false;
        }
        private void SideCanvas_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (dragObject == null)
                return;

            var pos = e.GetPosition(sender as IInputElement);

            if (pos.Y - dragOffset.Y < 0 || pos.Y + (40 - dragOffset.Y) > (sender as Canvas).ActualHeight)
                return;

            Canvas.SetTop(dragObject, pos.Y - dragOffset.Y);
            if (dragObject is UniversalPin pin)
            {
                Wire[] wires = pin.GetWires();
                for (int i = 0; i < wires.Length; i++)
                {
                    if (sender == canvasPinInput)
                        wires[i].Start = new Point(0, pos.Y - dragOffset.Y + 20);
                    else if (sender == canvasPinOutput)
                        wires[i].End = new Point(canvasMain.ActualWidth, pos.Y - dragOffset.Y + 20);
                    wires[i].UpdatePath();
                }
            }
        }

        private void Pin_StartWire(object sender, MouseEventArgs e)
        {
            if (wireState == Wire.State.NoWire)
                return;

            if (sender is UniversalPin uniPin)
            {
                Point pos = new Point(0, Canvas.GetTop(uniPin) + uniPin.ActualHeight / 2);

                Wire wire = new Wire(pos, wireState);

                if (uniPin.IsActive)
                    wire.ChangeState();

                uniPin.StateChanged += wire.ChangeState;

                canvasMain.Children.Add(wire);

                wire.PreviewMouseRightButtonDown += CanvasElement_ContextMenuCall;

                ((MenuItem)wire.contextMenu.Items[0]).Click += DeleteWireInContextMenu;
                ((MenuItem)wire.contextMenu.Items[2]).Click += Wire_ChangeColorToRed;
                ((MenuItem)wire.contextMenu.Items[3]).Click += Wire_ChangeColorToYellow;
                ((MenuItem)wire.contextMenu.Items[4]).Click += Wire_ChangeColorToGreen;
                ((MenuItem)wire.contextMenu.Items[5]).Click += Wire_ChangeColorToBlue;
                wire.contextMenu.Style = FindResource("contextMenuStyle") as Style;

                uniPin.AddWire(wire);

                drawWire = wire;
            }
        }
        private void Pin_RemoveWire(object sender, MouseEventArgs e)
        {
            if (drawWire == null) return;
            if (sender is UniversalPin pin)
            {
                pin.RemoveWire((Wire)canvasMain.Children[canvasMain.Children.Count - 1]);
            }
            drawWire = null;
        }
        private void Pin_NotActive(object sender, MouseEventArgs e)
        {
            dragObject = null;
        }
        private void Pin_PreviewMouseEnter(object sender, MouseEventArgs e)
        {
            if (drawWire == null) return;

            if (sender is UniversalPin pin)
            {
                drawWire.End = new Point(canvasMain.ActualWidth, Canvas.GetTop(pin) + pin.ActualHeight / 2);
                drawWire.UpdatePath();
                pin.AddWire(drawWire);
                drawWire.StateChanged += pin.ChangeState;
                pin.IsActive = drawWire.Active;
                drawWire = null;
            }
        }

        private void MainCanvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (sender is Canvas canvas)
            {
                if (drawWire != null)
                {
                    canvasMain.Children.RemoveAt(canvasMain.Children.Count - 1);
                    drawWire = null;
                }
            }
        }
        private void MainCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (drawWire == null) return;
            if (sender is Canvas canvas)
            {
                Point mousePos = e.GetPosition(canvasMain as IInputElement);

                ((Wire)canvasMain.Children[canvasMain.Children.Count - 1]).End = new Point(mousePos.X - 5, mousePos.Y);
                ((Wire)canvasMain.Children[canvasMain.Children.Count - 1]).UpdatePath();
            }
        }
        private void CanvasElement_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (wireState == Wire.State.NoWire)
            {
                dragObject = sender as UIElement;
                dragOffset = e.GetPosition(canvasMain);
                dragOffset.X -= Canvas.GetLeft(dragObject);
                dragOffset.Y -= Canvas.GetTop(dragObject);
            }
        }
        private void CanvasElement_PreviewMouseUpWithWire(object sender, MouseButtonEventArgs e)
        {
            if (drawWire == null) return;

            if (sender is Gate gate)
            {
                UniversalPin[] pins = gate.GetInputPins();

                if (pins.Length == 0) return;

                Point mousePos = e.GetPosition(gate);

                int pinPos = (int)(mousePos.Y / gate.canvas.Height * pins.Length);

                pins[pinPos].AddWire(drawWire);

                pins[pinPos].IsActive = drawWire.Active;

                drawWire.End = new Point(Canvas.GetLeft(gate) + 5, (int)(Canvas.GetTop(gate) + gate.canvas.Height / (pins.Length + 1) * (pinPos + 1)));
                drawWire.UpdatePath();

                drawWire.StateChanged += pins[pinPos].ChangeState;

                drawWire = null;
            }
        }
        private void CanvasElement_PreviewMouseDownStartWire(object sender, MouseButtonEventArgs e)
        {
            if (wireState == Wire.State.NoWire)
                return;

            if (e.LeftButton != MouseButtonState.Pressed)
                return;

            if (sender is Gate gate)
            {
                UniversalPin[] pins = gate.GetOutputPins();

                if (pins.Length == 0) return;

                Point mousePos = e.GetPosition(gate);

                int pinPos = (int)(mousePos.Y / gate.canvas.Height * pins.Length);

                drawWire = new Wire(new Point(Canvas.GetLeft(gate) + gate.canvas.Width - 5,
                    (int)(Canvas.GetTop(gate) + gate.canvas.Height / (pins.Length + 1) * (pinPos + 1))),
                    wireState);

                if (pins[pinPos].IsActive) 
                    drawWire.ChangeState();

                canvasMain.Children.Add(drawWire);

                pins[pinPos].AddWire(drawWire);

                drawWire.PreviewMouseRightButtonDown += CanvasElement_ContextMenuCall;

                ((MenuItem)drawWire.contextMenu.Items[0]).Click += DeleteWireInContextMenu;
                ((MenuItem)drawWire.contextMenu.Items[2]).Click += Wire_ChangeColorToRed;
                ((MenuItem)drawWire.contextMenu.Items[3]).Click += Wire_ChangeColorToYellow;
                ((MenuItem)drawWire.contextMenu.Items[4]).Click += Wire_ChangeColorToGreen;
                ((MenuItem)drawWire.contextMenu.Items[5]).Click += Wire_ChangeColorToBlue;
                drawWire.contextMenu.Style = FindResource("contextMenuStyle") as Style;

                pins[pinPos].StateChanged += drawWire.ChangeState;
            }
        }
        private void CanvasElement_ContextMenuCall(object sender, MouseEventArgs e)
        {
            contextMenuCaller = sender as UIElement;
        }
        private void MainCanvas_PreviewMouseMoveGate(object sender, MouseEventArgs e)
        {
            if (dragObject is Gate lg)
            {
                var pos = e.GetPosition(sender as IInputElement);
                if (pos.Y - dragOffset.Y < 0 || pos.Y + lg.ActualHeight - dragOffset.Y > canvasMain.ActualHeight
                    || pos.X - dragOffset.X < 0 || pos.X + lg.ActualWidth - dragOffset.X > canvasMain.ActualWidth)
                    return;
                Canvas.SetTop(lg, pos.Y - dragOffset.Y);
                Canvas.SetLeft(lg, pos.X - dragOffset.X);

                UniversalPin[] pins = lg.GetInputPins();
                for (int i = 0; i < pins.Length; i++)
                {
                    Wire[] wires = pins[i].GetWires();
                    for (int j = 0; j < wires.Length; j++)
                    {
                        wires[j].End = new Point(Canvas.GetLeft(dragObject) + 5,
                            Canvas.GetTop(dragObject) + (int)lg.ActualHeight / (pins.Length + 1) * (i + 1));
                        wires[j].UpdatePath();
                    }
                }

                pins = lg.GetOutputPins();
                for (int i = 0; i < pins.Length; i++)
                {
                    Wire[] wires = pins[i].GetWires();
                    for (int j = 0; j < wires.Length; j++)
                    {
                        wires[j].Start = new Point(Canvas.GetLeft(dragObject) + lg.ActualWidth - 5,
                            Canvas.GetTop(dragObject) + (int)lg.ActualHeight / (pins.Length + 1) * (i + 1));
                        wires[j].UpdatePath();
                    }
                }
            }            
        }
        private void Canvas_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            dragObject = null;
        }

        private void UniversalPinChangeState_MouseDown(object sender, MouseEventArgs e)
        {
            var pin = sender as UniversalPin;

            if(e.LeftButton == MouseButtonState.Pressed)
                if (wireState == Wire.State.NoWire)
                    pin.ChangeState();
        }
        private void UniversalPinRemovePin_MouseDown(object sender, MouseEventArgs e)
        {
            var pin = sender as UniversalPin;

            if (e.RightButton == MouseButtonState.Pressed)
            {
                canvasPinInput.Children.Remove(pin as UIElement);
                canvasPinOutput.Children.Remove(pin as UIElement);
                foreach (Wire wire in pin.GetWires())
                    canvasMain.Children.Remove(wire);
            }
        }
        private void DeleteGateInContextMenu(object sender, EventArgs e)
        {
            Gate lg = contextMenuCaller as Gate;

            UniversalPin[] pins = lg.GetInputPins();
            for (int i = 0; i < pins.Length; i++)
            {
                Wire[] wires = pins[i].GetWires();
                for (int j = 0; j < wires.Length; j++)
                {
                    canvasMain.Children.Remove(wires[j]);
                    wires[j].StateChanged -= pins[i].ChangeState;
                }
            }

            pins = lg.GetOutputPins();
            for (int i = 0; i < pins.Length; i++)
            {
                Wire[] wires = pins[i].GetWires();
                for (int j = 0; j < wires.Length; j++)
                {
                    canvasMain.Children.Remove(wires[j]);
                    pins[i].StateChanged -= wires[j].ChangeState;
                }
            }
            canvasMain.Children.Remove(lg);
        }
        private void DeleteWireInContextMenu(object sender, EventArgs e)
        {
            Wire w = contextMenuCaller as Wire;
            w.ClearEvent();
            canvasMain.Children.Remove(w);
        }

        private void OpenInfoPanel(object sender, EventArgs e)
        {
            Gate lg = contextMenuCaller as Gate;
            InformationPanel infoPanel = new InformationPanel(lg.GetName());
            infoPanel.ShowDialog();
        }

        public void AddGate(Gate gate)
        {
            gate.PreviewMouseDown += CanvasElement_PreviewMouseDown;
            gate.MouseUp += CanvasElement_PreviewMouseUpWithWire;
            gate.MouseDown += CanvasElement_PreviewMouseDownStartWire;
            gate.PreviewMouseRightButtonDown += CanvasElement_ContextMenuCall;
            canvasMain.Children.Add(gate);
            Canvas.SetTop(gate, 10);
            Canvas.SetLeft(gate, 10);
            ((MenuItem)gate.contextMenu.Items[0]).Click += DeleteGateInContextMenu;
            ((MenuItem)gate.contextMenu.Items[1]).Click += OpenInfoPanel;
            gate.contextMenu.Style = FindResource("contextMenuStyle") as Style;
        }

        public void ClearScene()
        {
            canvasMain.Children.Clear();
            canvasPinInput.Children.Clear();
            canvasPinOutput.Children.Clear();
        }

        private void Wire_ChangeColorToRed(object sender, EventArgs e)
        {
            if (contextMenuCaller is Wire w)
            {
                w.ChangeColor(Wire.ColorPallete.Red);
                contextMenuCaller = null;
            }
        }
        private void Wire_ChangeColorToYellow(object sender, EventArgs e)
        {
            if (contextMenuCaller is Wire w)
            {
                w.ChangeColor(Wire.ColorPallete.Yellow);
            }
        }
        private void Wire_ChangeColorToBlue(object sender, EventArgs e)
        {
            if (contextMenuCaller is Wire w)
            {
                w.ChangeColor(Wire.ColorPallete.Blue);
                contextMenuCaller = null;
            }
        }
        private void Wire_ChangeColorToGreen(object sender, EventArgs e)
        {
            if (contextMenuCaller is Wire w)
            {
                w.ChangeColor(Wire.ColorPallete.Green);
                contextMenuCaller = null;

            }
        }
    }
}
