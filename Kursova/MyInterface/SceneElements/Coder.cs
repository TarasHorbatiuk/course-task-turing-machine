﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Text.RegularExpressions;
using System;
using System.IO;

namespace Kursova.MyInterface.SceneElements
{
    internal class Coder : Gate
    {
        public Coder()
            : base(0, 8, Colors.Khaki, "CODER")
        {
            canvas.Width = 120;
            TextBox textBox = new TextBox();
            textBox.FontSize = 22;
            textBox.Text = "0";

            textBox.PreviewTextInput += TextBox_TextChanged;
            textBox.TextChanged += TextBox_TextChanged;

            textBox.Width = 50;
            textBox.Height = 25;

            Canvas.SetTop(textBox, canvas.Height / 2 - textBox.Height / 2);
            Canvas.SetLeft(textBox, canvas.Width / 2 - textBox.Width / 2);

            canvas.Children.Add(textBox);
        }

        private void TextBox_TextChanged(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !char.IsDigit(e.Text[0]);
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if((sender as TextBox).Text == "")
                for (int i = 0; i < 8; i++)
                    outs[i].IsActive = false;

            if (short.TryParse((sender as TextBox).Text, out short num))
            {
                if (num > 255)
                {
                    (sender as TextBox).Text = "255";
                    return;
                }
                char[] values = new char[8];
                string binary = Convert.ToString(num, 2);

                for (int i = 0; i < 8; i++)
                    if (i < binary.Length)
                        values[i] = binary[binary.Length - i - 1];
                    else
                        values[i] = '0';

                for (int i = 0; i < 8; i++)
                    if (i < binary.Length)
                        outs[i].IsActive = (values[i] == '1');
                    else
                        outs[i].IsActive = false;
            }
        }
    }
}
