﻿using System.Windows.Media;

namespace Kursova.MyInterface.SceneElements
{
    internal class SevenDigitDisplay : Gate
    {
        public SevenDigitDisplay()
            : base(7, 0, Colors.Gray, "7-S Display")
        {
            Wire[] wires = new Wire[7]
               {
                    new Wire(new System.Windows.Point(40, 20), new System.Windows.Point(100, 20), Wire.State.StraightWire),
                    new Wire(new System.Windows.Point(100, 20), new System.Windows.Point(100, 75), Wire.State.StraightWire),
                    new Wire(new System.Windows.Point(100, 75), new System.Windows.Point(100, 130), Wire.State.StraightWire),
                    new Wire(new System.Windows.Point(100, 130), new System.Windows.Point(40, 130), Wire.State.StraightWire),
                    new Wire(new System.Windows.Point(40, 130), new System.Windows.Point(40, 75), Wire.State.StraightWire),
                    new Wire(new System.Windows.Point(40, 75), new System.Windows.Point(40, 20), Wire.State.StraightWire),
                    new Wire(new System.Windows.Point(40, 75), new System.Windows.Point(100, 75), Wire.State.StraightWire)
               };

            canvas.Width = 120;

            for (int i = 0; i < 7; i++)
            {
                wires[i].path.StrokeThickness = 8;
                canvas.Children.Add(wires[i]);
                (canvas.Children[i] as UniversalPin).StateChanged += wires[i].ChangeState;
                wires[i].UpdatePath();
            }
        }
    }
}
