﻿using System;
using System.Windows.Controls;
using System.Windows.Media;

namespace Kursova.MyInterface
{
    /// <summary>
    /// Interaction logic for Gate.xaml
    /// </summary>
    public abstract partial class Gate : UserControl
    {
        protected UniversalPin[] ins;
        protected UniversalPin[] outs;
        protected string name;
        public Gate()
        {
            ins = null;
            outs = null;
            InitializeComponent();
        }
        public Gate(int inCount, int outCount, Color backColor, string name)
        {
            InitializeComponent();
            ins = new UniversalPin[inCount];
            outs = new UniversalPin[outCount];
            Background = new SolidColorBrush(backColor);
            this.name = name;

            canvas.Height = Math.Max(inCount, outCount) * 20 + 15;
            canvas.Width = 50;

            for (int i = 0; i < inCount; i++)
            {
                ins[i] = new UniversalPin(false, 10);
                canvas.Children.Add(ins[i]);
                Canvas.SetLeft(ins[i], 5);
                Canvas.SetTop(ins[i], canvas.Height / (inCount + 1) * (i + 1) - 5);
            }
            for (int i = 0; i < outCount; i++)
            {
                outs[i] = new UniversalPin(false, 10);
                canvas.Children.Add(outs[i]);
                Canvas.SetRight(outs[i], 5);
                Canvas.SetTop(outs[i], canvas.Height / (outCount + 1) * (i + 1) - 5);
            }
        }
        public UniversalPin[] GetInputPins() { return ins; }
        public UniversalPin[] GetOutputPins() { return outs; }
        public string GetName() { return name; }
    }
}
