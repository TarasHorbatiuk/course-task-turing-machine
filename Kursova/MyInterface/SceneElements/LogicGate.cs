﻿using System;
using System.Windows.Media;
using System.Windows.Controls;

namespace Kursova.MyInterface.SceneElements
{
    public partial class LogicGate : Gate
    {
        Func<bool[], bool[]> function;
        public LogicGate() : base()
        {
            function = null;
        }
        public LogicGate(int inCount, int outCount, string Name, Func<bool[], bool[]> function, Color backColor)
            : base(inCount, outCount, backColor, Name)
        {
            this.function = function;
            Label label = new Label();

            label.Content = Name;
            label.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            label.VerticalContentAlignment = System.Windows.VerticalAlignment.Center;
            label.FontFamily = new FontFamily("Cascadia Code");
            label.FontSize = 22;
            canvas.Width = Name.Length * 20 + 50;
            label.Height = canvas.Height;
            label.Width = canvas.Width;

            foreach (UniversalPin pin in ins)
                pin.StateChanged += UpdateState;

            canvas.Children.Add(label);
            UpdateState();
        }

        public void UpdateState()
        {
            bool[] x = new bool[ins.Length];
            for (int i = 0; i < ins.Length; i++)
                x[i] = ins[i].IsActive;
            bool[] results = function(x);

            for (int i = 0; i < outs.Length; i++)
                outs[i].IsActive = results[i];
        }
        public Func<bool[], bool[]> GetFunction() { return function; }
    }
}
