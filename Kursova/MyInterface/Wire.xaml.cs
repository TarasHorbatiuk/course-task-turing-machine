﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Kursova.MyInterface
{
    /// <summary>
    /// Interaction logic for Wire.xaml
    /// </summary>
    public partial class Wire : UserControl
    {
        public enum State
        {
            NoWire,
            StraightWire,
            CurvedWire
        }

        public enum ColorPallete
        {
            Red, 
            Yellow, 
            Green,
            Blue
        }

        bool active;
        public bool Active { get => active; }

        Point startPosition;
        Point endPosition;
        State wireDrawState;
        public event StateEventHandler StateChanged;

        Color passiveColor;
        Color activeColor;

        public State DrawState {
            get => wireDrawState;
            set => wireDrawState = value;
        }
        public Point Start
        {
            get => startPosition;
            set => startPosition = value;
        }
        public Point End
        {
            get => endPosition;
            set => endPosition = value;
        }

        public Wire()
        {
            InitializeComponent();
            active = false;
            passiveColor = Colors.DarkRed;
            activeColor = Colors.Red;
        }
        public Wire(Point startPosition) : this()
        {
            this.startPosition = startPosition;
            endPosition = startPosition;
        }
        public Wire(Point startPosition, State drawState) : this(startPosition)
        {
            wireDrawState = drawState;
        }
        public Wire(Point startPosition, Point endPosition, State drawState) : this()
        {
            this.startPosition = startPosition;
            this.endPosition = endPosition;
            wireDrawState = drawState;
        }

        protected virtual void StateChangedCallEvent()
        {
            if (StateChanged != null)
                StateChanged.Invoke();
        }
        public void ChangeState()
        {
            active = !active;
            if (active)
                path.Stroke = new SolidColorBrush(activeColor);
            else
                path.Stroke = new SolidColorBrush(passiveColor);

            StateChangedCallEvent();
        }
        public void UpdatePath()
        {
            startPosition = new Point(Math.Floor(startPosition.X), Math.Floor(startPosition.Y));
            endPosition = new Point(Math.Floor(endPosition.X), Math.Floor(endPosition.Y));

            if (wireDrawState == State.StraightWire)
            {
                path.Data = Geometry.Parse(
                    $"M {startPosition.X},{startPosition.Y} " +
                    $"L {startPosition.X},{startPosition.Y} " +
                    $"{endPosition.X},{endPosition.Y}"
                    );
                return;
            }

            double totalWidth = endPosition.X - startPosition.X;
            double totalHeight = endPosition.Y - startPosition.Y;

            double HO = totalWidth > 0 ? 1 : -1;
            double VO = totalHeight > 0 ? 1 : -1;

            double curveSize = 20;

            if (Math.Abs(totalWidth) < Math.Abs(totalHeight))
            {
                if (Math.Abs(totalWidth) < 100)
                    curveSize = Math.Ceiling(totalWidth / 5);
            }
            else
            {
                if (Math.Abs(totalHeight) < 100)
                    curveSize = Math.Ceiling(totalHeight / 5);
            }
            curveSize = Math.Abs(curveSize);

            double HLineSize = Math.Ceiling((totalWidth - 2 * curveSize * HO) / 2);
            double VLineSize = totalHeight - 2 * curveSize * VO;

            if(curveSize < 2)
            {
                path.Data = Geometry.Parse(
                    $"M {startPosition.X},{startPosition.Y} " +
                    $"L {startPosition.X},{startPosition.Y} " +
                    $"{endPosition.X},{endPosition.Y}"
                    );
                return;
            }

            path.Data = Geometry.Parse(
                $"M {startPosition.X},{startPosition.Y}" +
                $" H {startPosition.X + HLineSize}" +
                $" C {startPosition.X + HLineSize},{startPosition.Y} " +
                    $"{startPosition.X + HLineSize + curveSize * HO},{startPosition.Y} " +
                    $"{startPosition.X + HLineSize + curveSize * HO},{startPosition.Y + curveSize * VO}" +
                $" V {startPosition.Y + curveSize * VO + VLineSize}" +
                $" C {startPosition.X + HLineSize + curveSize * HO},{startPosition.Y + VLineSize + curveSize * VO} " +
                    $"{startPosition.X + HLineSize + curveSize * HO},{startPosition.Y + VLineSize + 2 * curveSize * VO} " +
                    $"{startPosition.X + HLineSize + 2 * curveSize * HO},{startPosition.Y + VLineSize + 2 * curveSize * VO}" +
                $" H {startPosition.X + 2 * HLineSize + 2 * curveSize * HO}"
                );
        }
        public void ChangeColor(ColorPallete pallete)
        {
            switch (pallete)
            {
                case ColorPallete.Red:
                    passiveColor = Colors.DarkRed;
                    activeColor = Colors.Red;
                    break;
                case ColorPallete.Yellow:
                    passiveColor = Colors.Brown;
                    activeColor = Colors.Yellow;
                    break;
                case ColorPallete.Green:
                    passiveColor = Colors.DarkGreen;
                    activeColor = Colors.Lime;
                    break;
                case ColorPallete.Blue:
                    passiveColor = Colors.Navy;
                    activeColor = Colors.Aqua;
                    break;
            }
            if (active)
                path.Stroke = new SolidColorBrush(activeColor);
            else
                path.Stroke = new SolidColorBrush(passiveColor);
        }
        public void ClearEvent()
        {
            StateChanged = null;
        }
    }
}
