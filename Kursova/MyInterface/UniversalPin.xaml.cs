﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Kursova.MyInterface
{
    /// <summary>
    /// Interaction logic for UniversalPin.xaml
    /// </summary>
    public delegate void StateEventHandler();
    public partial class UniversalPin : UserControl
    { 
        public event StateEventHandler StateChanged; 
        private Wire[] wires;
        public bool IsActive
        {
            get { return (bool)GetValue(IsActiveProperty); }
            set {
                bool temp = (bool)GetValue(IsActiveProperty);
                SetValue(IsActiveProperty, value);            
                if (value != temp)
                    StateChangedCallEvent();
            }
        }

        public int Diameter
        {
            get { return (int)GetValue(DiameterProperty); }
            set { SetValue(DiameterProperty, value); }
        }

        public UniversalPin(bool isActive, int diametr)
        {
            InitializeComponent();
            IsActive = isActive;
            Diameter = diametr;
            wires = new Wire[0];
        }

        public static readonly DependencyProperty IsActiveProperty = DependencyProperty.Register(
            "IsActive", typeof(bool), typeof(UniversalPin));

        public static readonly DependencyProperty DiameterProperty = DependencyProperty.Register(
            "Diameter", typeof(int), typeof(UniversalPin));

        public void ChangeState()
        {
            IsActive = !IsActive;
        }

        protected virtual void StateChangedCallEvent()
        {
            if (StateChanged != null)
                StateChanged();
        }

        public void AddWire(Wire w)
        {
            wires = wires.Append(w).ToArray();
        }
        public void RemoveWire(Wire w)
        {
            wires = wires.Except(new Wire[] { w }).ToArray();
        }
        public Wire[] GetWires()
        {
            return wires;
        }
    }
}
