﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Kursova.MyInterface.SceneElements;

namespace Kursova.MyInterface
{
    /// <summary>
    /// Interaction logic for InformationPanel.xaml
    /// </summary>
    public partial class InformationPanel : Window
    {
        public InformationPanel()
        {
            InitializeComponent();
        }
        public InformationPanel(string name)
        {
            InitializeComponent();
            for (int i = 0; i < listBox.Items.Count; i++)
                if ((listBox.Items[i] as ListBoxItem).Content.Equals(name))
                    listBox.SelectedIndex = i;
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ListBoxItemNOT_Selected(object sender, RoutedEventArgs e)
        {
            FillWindowWithInfoLogicGate(GateBuider.CreateGate_NOT());
        }
        private void ListBoxItemAND_Selected(object sender, RoutedEventArgs e)
        {
            FillWindowWithInfoLogicGate(GateBuider.CreateGate_AND());
        }
        private void ListBoxItemOR_Selected(object sender, RoutedEventArgs e)
        {
            FillWindowWithInfoLogicGate(GateBuider.CreateGate_OR());
        }
        private void ListBoxItemXOR_Selected(object sender, RoutedEventArgs e)
        {
            FillWindowWithInfoLogicGate(GateBuider.CreateGate_XOR());
        }
        private void ListBoxItemNOR_Selected(object sender, RoutedEventArgs e)
        {
            FillWindowWithInfoLogicGate(GateBuider.CreateGate_NOR());
        }
        private void ListBoxItemNAND_Selected(object sender, RoutedEventArgs e)
        {
            FillWindowWithInfoLogicGate(GateBuider.CreateGate_NAND());
        }
        private void ListBoxItemXNOR_Selected(object sender, RoutedEventArgs e)
        {
            FillWindowWithInfoLogicGate(GateBuider.CreateGate_XNOR());
        }
        private void ListBoxItemADDER_Selected(object sender, RoutedEventArgs e)
        {
            FillWindowWithInfoLogicGate(GateBuider.CreateGate_ADDER());
        }
        private void ListBoxItemDisplay_Selected(object sender, RoutedEventArgs e)
        {
            labelName.Content = "Display";
            gridForGate.Children.Clear();
            gridForGate.Children.Add(GateBuider.CreateDisplay());

            gridTable.RowDefinitions.Clear();
            gridTable.ColumnDefinitions.Clear();
            gridTable.Children.Clear();

            Label label = new Label();
            gridTable.Children.Add(label);
            label.FontSize = 22;
            label.Content = "Дисплей з 7 сегментами. Кожен \nвхід відповідає за свій сегмент";
        }
        private void ListBoxItemCODER_Selected(object sender, RoutedEventArgs e)
        {
            labelName.Content = "CODER";
            gridForGate.Children.Clear();
            gridForGate.Children.Add(GateBuider.CreateCoder());

            gridTable.RowDefinitions.Clear();
            gridTable.ColumnDefinitions.Clear();
            gridTable.Children.Clear();

            Label label = new Label();
            gridTable.Children.Add(label);
            label.FontSize = 22;
            label.Content = "Перетворює вхідне число [0;255] \nв бінарний сигнал";
        }
        private void ListBoxItemGPU1_Selected(object sender, RoutedEventArgs e)
        {
            labelName.Content = "GPU-1";
            gridForGate.Children.Clear();
            gridForGate.Children.Add(GateBuider.CreateGate_GPU1());

            gridTable.RowDefinitions.Clear();
            gridTable.ColumnDefinitions.Clear();
            gridTable.Children.Clear(); 
            
            Label label = new Label();
            gridTable.Children.Add(label);
            label.FontSize = 22;
            label.Content = "Використовується для перетворення \nбінарного сигналу в сигнал \nдля 7-сегментного дисплея";
        }
        private void FillWindowWithInfoLogicGate(LogicGate gate)
        {
            labelName.Content = gate.GetName();
            gridForGate.Children.Clear();
            gridForGate.Children.Add(gate);

            UniversalPin[] ins = gate.GetInputPins();
            UniversalPin[] outs = gate.GetOutputPins();
            Func<bool[], bool[]> func = gate.GetFunction();

            gridTable.RowDefinitions.Clear();
            gridTable.ColumnDefinitions.Clear();
            gridTable.Children.Clear();

            for (int i = 0; i < ins.Length + outs.Length + 1; i++)
            {
                gridTable.RowDefinitions.Add(new RowDefinition());
                gridTable.RowDefinitions[i].Height = new GridLength(30);
            }

            gridTable.RowDefinitions[ins.Length].Height = new GridLength(5);

            for (int i = 0; i < Math.Pow(2, ins.Length) + 1; i++)
            {
                gridTable.ColumnDefinitions.Add(new ColumnDefinition());
                gridTable.ColumnDefinitions[i].Width = new GridLength(30);
            }

            for (int i = 0; i < gridTable.RowDefinitions.Count; i++)
            {
                for (int j = 0; j < gridTable.ColumnDefinitions.Count; j++)
                {
                    var br = new Border();
                    br.BorderBrush = new SolidColorBrush(Colors.Black);
                    br.BorderThickness = new Thickness(2);
                    gridTable.Children.Add(br);
                    Grid.SetRow(br, i);
                    Grid.SetColumn(br, j);

                    if (i == ins.Length)
                        br.Background = new SolidColorBrush(Colors.Black);

                    if (j == 0)
                    {
                        string alfhabet = "ABCDEFGH";
                        Label label = new Label();
                        label.FontSize = 16;

                        label.HorizontalAlignment = HorizontalAlignment.Center;

                        if (i < ins.Length)
                        {
                            label.Content = alfhabet[i];
                            br.Background = new SolidColorBrush(Colors.White);
                        }
                        else if (i > ins.Length)
                        {
                            label.Content = alfhabet[i - ins.Length - 1];
                            br.Background = new SolidColorBrush(Colors.White);
                        }

                        br.Child = label;
                    }
                    else
                    {
                        bool[] x = new bool[ins.Length];
                        for (int k = 0; k < ins.Length; k++)
                        {
                            x[k] = ((j - 1) >> k & 1) == 1;
                            if (k == i)
                            {                               
                                if (x[k])
                                    br.Background = new SolidColorBrush(Colors.Red);
                                else
                                    br.Background = new SolidColorBrush(Colors.White);
                            }
                        }
                        bool[] results = func(x);

                        for (int k = 0; k < outs.Length; k++)
                            if (k + ins.Length + 1 == i)
                            {
                                if (results[k])
                                    br.Background = new SolidColorBrush(Colors.Red);
                                else
                                    br.Background = new SolidColorBrush(Colors.White);
                            }
                    }
                }
            }
        }
    }
}
